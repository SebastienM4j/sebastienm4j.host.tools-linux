#!/bin/sh

SCRIPTDIR=$(cd "$(dirname "$0")"; pwd)

# http://askubuntu.com/questions/283908/how-can-i-install-and-use-powerline-plugin
# http://powerline.readthedocs.io/en/master/installation.html

# Install powerline
sudo apt-get install python-pip
su -c 'pip install git+git://github.com/Lokaltog/powerline'

# Install by apt-get, but it seem's does not work...
#sudo apt-get install powerline


# Install fonts symbols
sudo wget https://github.com/Lokaltog/powerline/raw/develop/font/PowerlineSymbols.otf
sudo mv PowerlineSymbols.otf /usr/share/fonts/
sudo fc-cache -vf

sudo wget https://github.com/Lokaltog/powerline/raw/develop/font/10-powerline-symbols.conf
sudo mv 10-powerline-symbols.conf /etc/fonts/conf.d/

# For patched fonts
#git clone https://github.com/powerline/fonts.git
#fonts/install.sh
#rm -Rf fonts/


# Set powerline for user
sudo cp "$SCRIPTDIR/bash.bashrc" /etc/bash.bashrc
cp -f "$SCRIPTDIR/../home/.bashrc" ~/

# Add 'source #/usr/share/powerline/bindings/bash/powerline.sh' in
# .bashrc for user installation
# /etc/bash.bashrc for system installation


# Set configuration
sudo cp -Rf "$SCRIPTDIR/config_files/*" /usr/local/lib/python2.7/dist-packages/powerline/config_files/

