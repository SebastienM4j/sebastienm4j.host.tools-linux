#!/bin/sh

BASEDIR=$(dirname "$0")
cd "$BASEDIR"
BASEDIR=$(pwd)


# Traitement des arguments

while [ "$1" != "" ]; do
	case $1 in
		-r | --restore )
			ACTION="R"
			;;

		-b | --backup )
			ACTION="B"
			;;

		-p | --profile )
			shift
			PROFILE=$1
			;;

		-h | --home )
			shift
			HOMEDIR=$1
			;;

		*)
			echo "Unknowed parameter $1"
			exit 1
	esac
	shift	
done

if [ -z "$HOMEDIR" ]; then
        echo "Home directory is required (option -h or --home)"
        exit 1
fi

case $PROFILE in
	home | apologic)
		;;

	*)
		echo "Unknowed profile, choose between : home, apologic"
		exit 1
esac
	

# Restauration/Backup des fichiers de config

if [ $ACTION = "R" ]; then
	SRCDIR="$BASEDIR"
	DSTDIR="$HOMEDIR"
	echo "Restore configuration from '$SRCDIR' to '$DSTDIR'"
else
	SRCDIR="$HOMEDIR"
	DSTDIR="$BASEDIR"
	echo "Backup configuration from '$SRCDIR' to '$DSTDIR'"
fi


# bashrc
cp -f "$SRCDIR/.bashrc" "$DSTDIR/.bashrc"
cp -f "$SRCDIR/.bashrc_prompt" "$DSTDIR/.bashrc_prompt"
cp -f "$SRCDIR/.bashrc_import" "$DSTDIR/.bashrc_import"
cp -f "$SRCDIR/.bashrc_import_$PROFILE" "$DSTDIR/.bashrc_import_$PROFILE"

# terminator
mkdir -p "$DSTDIR/.config/terminator"
cp -f "$SRCDIR/.config/terminator/config" "$DSTDIR/.config/terminator/config"

# git
if [ $ACTION = "R" ]; then
	cp -f "$SRCDIR/.gitconfig_$PROFILE" "$DSTDIR/.gitconfig"
else
	cp -f "$SRCDIR/.gitconfig" "$DSTDIR/.gitconfig_$PROFILE"
fi
cp -f "$SRCDIR/.git-credentials" "$DSTDIR/.git-credentials"
cp -f "$SRCDIR/.gitignore" "$DSTDIR/.gitignore"
cp -f "$SRCDIR/.git-ignore-global" "$DSTDIR/.git-ignore-global"

# vim
cp -f "$SRCDIR/.vimrc" "$DSTDIR/.vimrc"

